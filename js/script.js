"use strict";
document.addEventListener("DOMContentLoaded", function(){
	if (namePano){
		var Pano = new HwPano();
		var PanoCreater = new HwPanoCreater();
		var allTargetInfo = [];
		var explosion = document.querySelector('.target_boom');
		var explosionSrc = explosion.src; explosion.src = "";
		
		
		Pano.set('pano/'+namePano);
		Pano.TargetFunc.max = function(e){console.log(1111)};
		
		
		PanoCreater.hw_ajax({
			url:'pano/'+namePano+'/target/target.json',
			type:'GET',
			convert:'JSON',
			fc:function(info){
				if (info.length){
					info.forEach(function(item){
						allTargetInfo.push(item);
					});
				}
			}
		});
		
		
		var panoBlock = document.querySelector('.pano');
		var allClick = document.querySelectorAll('.click');
		var targetSet = document.querySelector('.target_settings');
		var buttonClick1 = allClick[0];
		var buttonClick2 = allClick[1];
		var buttonClick3 = allClick[2];
		var buttonClick4 = allClick[3];

		buttonClick1.addEventListener('click',function(){
			var Etop = document.querySelector('.target_top input').value || 0;
			var Eleft = document.querySelector('.target_left input').value || 0;
			var width = document.querySelector('.target_width input').value || 0;
			var height = document.querySelector('.target_height input').value || 0;
			
			var Eclass = document.querySelector('.target_class input').value || 0;
			var data = document.querySelector('.target_data input').value || 0;
			
			var css = document.querySelector('.target_css textarea').value || 0;
			var content = document.querySelector('.target_content textarea').value || 0;
			
			var imgsrc = document.querySelector('.target_img_src input').value || 0;
			var imgcss = document.querySelector('.target_img_css input').value || 0;
			
			var nextPano = document.querySelector('.target_nextPano input').value || 0;
			var func = document.querySelector('.target_func input').value || 0;

			var set = {};
				if(Etop){set.top = Etop;}
				if(Eleft){set.left = Eleft;}
				if(width){set.width = width;}
				if(height){set.height = height;}
				if(css){set.css = css;}
				if(Eclass){set.class = Eclass;}
				if(content){set.content = content;}
				if(imgsrc){set.img = {}; set.img.src = imgsrc;}
				if(imgcss){set.img.css = imgcss;}
				if(nextPano){set.nextPano = nextPano;}
				if(func){set.func = func;}
				if(data){set.data = data;}

			var vector = PanoCreater.CreateTarget(Pano,set);
				set.vect = vector;
				
			allTargetInfo.push(set);
				
				
			explosion.src = explosionSrc;
			explosion.style.display = "block";
			setTimeout(function(){explosion.style.display = "none";explosion.src = "";},(explosion.dataset.time*1000));
		});
		
		buttonClick2.addEventListener('click',function(){
			if (targetSet.style.display == "none") {targetSet.style.display = "block";}
			else {targetSet.style.display = "none";}
		});
		
		buttonClick3.addEventListener('click',function(){
			PanoCreater.hw_ajax({
				url:'download_target.php',
				type:'POST',
				info:'type=download&name='+namePano,
				fc:function(info){				
					window.location.href = "/hw-three-pano-creater/pano/"+namePano+"/"+info;
				}
			});
		});
		
		buttonClick4.addEventListener('click',function(){
			var allTarget=0;
			var jsonString = JSON.stringify(allTargetInfo);
			allTarget = 'name='+namePano+'&json='+jsonString;

			PanoCreater.hw_ajax({
				url:'save_target.php',
				type:'POST',
				info:allTarget,
				fc:function(info){
					console.log(info);
					window.location.href ='?name='+namePano;
				}
			});
		});
	}
});