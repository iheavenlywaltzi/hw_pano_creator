"use strict";
function HwPanoCreater (){	

	function UnScreenPosition(camera){
		var vector = new THREE.Vector3(0,0,0.9);   
		vector.unproject(camera);
		return vector;
	};
	
	function ScreenPosition(v,camera,width2,height2){
		var x = v.x || 0, y = v.y || 0, z = v.z || 0;
		var vector = new THREE.Vector3(x,y,z);
		vector.project(camera);
		vector.x = ( vector.x * width2 ) + width2;
		vector.y = - ( vector.y * height2 ) + height2;
		return { 
			x: vector.x,
			y: vector.y,
			z: vector.z
		}
	};
	
	function CreateTarget (pano,set) {
		var camera = pano.global.pano.camera
		var vector = UnScreenPosition(camera);
		var position = ScreenPosition(vector,camera,pano.global.canvas.width2,pano.global.canvas.height2);
		var width = parseInt(set.width) || 0, height = parseInt(set.height) || 0;
		var DT = parseInt(set.top) || 0, DL = parseInt(set.left) || 0;
		var css = set.css || 0;
		var classe = set.class || 0;
		var content = set.content || 0;
		
		var NewDiv = document.createElement('div');
			if(classe){
				if (classe.indexOf(' ') != -1){
					var arr = classe.split(' ');
					arr.forEach(function(item){NewDiv.classList.add(item);});
				}
				else {NewDiv.classList.add(classe);}
			}
			if(css){NewDiv.style.cssText = css;}
			NewDiv.style.position = 'absolute';
			NewDiv.style.top = position.y+'px';
			NewDiv.style.left = position.x+'px';
			if (width){NewDiv.style.width = width+'px';};
			if (height){NewDiv.style.height = height+'px';};
			
			if (content){NewDiv.innerHTML = content;};

		pano.global.pano.panoTarget.appendChild(NewDiv);
		pano.global.allTarget.push({e:NewDiv,v:{x:vector.x,y:vector.y,z:vector.z},t:DT,l:DL});
		return vector;
	}
	
	this.CreateTarget = CreateTarget;
	
	function hw_ajax (option){
		var XHR = ("onload" in new XMLHttpRequest()) ? XMLHttpRequest : XDomainRequest;
		
		var url = option.url || '';
		var type = option.type || 'GET'; 
		var info = option.info || ''; 
		var convert = option.convert || '';
		var fc = option.fc || function(){};
		var err = option.err || function(){};
		
		var dataInfo = 'None date!!';

		var request = new XHR();
		request.onreadystatechange = function() {
			if(request.readyState === 4) {
				if(request.status === 200) { 
					dataInfo = request.responseText
					if (convert == 'JSON') {dataInfo = JSON.parse(dataInfo)}
					fc(dataInfo);
				} else {err();}
			}
		}
		var ver = new Date().getTime();
		if (type == 'GET'){
			request.open(type, url+"?ver="+ver);
			request.send();
		} else if (type == 'POST'){
			request.open(type, url+"?ver="+ver);
			request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')
			request.send(info);
		}
		
	}
	this.hw_ajax = hw_ajax;

}