"use strict";
function HwPano (options){	
	var object = this;
	var global={};
	global.pano={}; // объект с самим пано
	global.click = {}; // объектс с кординатами нажатий
	global.control = {}// объект с контроллерами
	global.allTarget = [];
	if (!options){options={};}
	global.selector = options.selector || '.pano';
	var zoomFunct = options.zoomFunct || function(){};

	global.control.zoomMax = 3.5; // Максимальное увелечение 
	global.control.zoomMin = 0.5; // Максимальное уменьшение
	global.control.zoomTouchKof = 0.002;// коофицент скорости увелечения зума от пальцев
	global.control.zoomMousKof = 0.0005;// коофицент скорости увелечения зума от мышки
	global.control.rotationSpeed = 0.001; // скорость вращения панорамы
	global.control.yMax = 1.5; // максимальный угол наклона веерх и низ

	InitThree();
	Animate();
	
	this.global = global;
	
	function InitThree (){
		global.doc = document;
		var panoBlock = global.doc.querySelector(global.selector);
			panoBlock.style.overflow = "hidden";
			panoBlock.style.position = "relative";
			global.pano.panoBlock = panoBlock;
		var panoTarget = global.doc.createElement('div');
			panoTarget.classList.add("Hw-three-pano-target");
			panoTarget.style.position = "absolute";
			panoTarget.style.top = "0";
			panoTarget.style.left = "0";
			panoTarget.style.height = "0";
			panoTarget.style.width = "0";
			panoTarget.style.zIndex = "1";
			global.pano.panoTarget = panoTarget;
		var canvas = global.doc.createElement('canvas');
			canvas.classList.add("Hw-three-pano");
			canvas.style.position = "absolute";
			canvas.style.top = "0";
			canvas.style.left = "0";
			canvas.style.height = "100%";
			canvas.style.width = "100%";
			canvas.style.zIndex = "0";
			hw_disable_touch(canvas);
		panoBlock.appendChild(canvas);
		panoBlock.appendChild(panoTarget);
		global.canvas = canvas;
		
		global.canvas.width = global.canvas.clientWidth; 
		global.canvas.height = global.canvas.clientHeight;
		global.canvas.width2 = global.canvas.clientWidth/2;
		global.canvas.height2 = global.canvas.clientHeight/2;
		
		var fov = 75, aspect = global.canvas.clientWidth/global.canvas.clientHeight, near = 0.1, far = 2000;
			
		global.pano.scene = new THREE.Scene(); // создания сцены
		global.pano.camera = new THREE.PerspectiveCamera(fov,aspect,near,far); // создание камеры.
			global.pano.camera.position.set(0, 0, 0); // установим камеру в определенную позицию
			global.pano.camera.rotation.order = "YZX";
			
		global.pano.renderer = new THREE.WebGLRenderer({canvas:global.canvas});
			global.pano.renderer.setSize(global.canvas.width,global.canvas.height)
			global.pano.renderer.clearColor();	
			
		global.canvas.addEventListener('mousedown',CanvasClick);
		global.canvas.addEventListener('touchstart',CanvasClick);
		global.canvas.addEventListener("wheel",CameraWheel);
		THREE.Cache.clear();
	}
	
	global.delSeg = [];
	function CteateSphere(Options={}){ //  создаст сферу по заданным параметрам
		THREE.Cache.clear();
		if (!global.geometry) (global.geometry={});
		global.geometry.type = "sphere";
		global.delSeg = global.delSeg.concat(global.geometry.allSegment);
		global.geometry.allSegment = [];
		
		var AllLine = Options.line || 4; // количество линий
		var SegmentLine = Options.segment || 8; // количество сегментов в 1й линии
		var vertex = Options.vertex || 16; // количество точкер в 1м сегменте
		var radius = Options.radius || 5; // радиус сферы
		var scale = Options.scale || [-1,1,1]; // размерность сферы
		var color = Options.color || [0,0,0]; // цвет всей сферы
		var cbFunct = Options.cb || function(){};
		
		var pish = (Math.PI*2); // 1 полный оборот ( горизонтальный )
		var shsi = pish/SegmentLine; // Размер сегмента по горизонтали ( право лево)
		
		var pisv = Math.PI// 1 полный оборот ( вертикальный )
		var svsi = pisv/AllLine; //  Размер сегмента по вертикали ( верх низ)
		var SI = 0;
		for (var il = 0; il < AllLine; il++){	
			var posL = (pisv*il)/AllLine; 
			for (var is = 0;is < SegmentLine;is++){
				var posS = (pish*is)/SegmentLine; // позиция сегмента
				var img = 0, texture = 0,r=0,g=0,b=0;

				if (color == 'random') {r=parseInt(Math.random()*100) , g=parseInt(Math.random()*100) , b=parseInt(Math.random()*100);}
				else {r=(color[0]); g=(color[1]); b=(color[2]); }
				texture = new THREE.MeshBasicMaterial( { color: "rgb(0%, 0%, 0%)",opacity:0,transparent:true });

				var geometry = new THREE.SphereGeometry( radius, vertex, vertex, posS, shsi, posL, svsi); // создадим геометрию
					geometry.scale( scale[0], scale[1], scale[2] ); // инвертнем геометрию
				var obgect = new THREE.Mesh(geometry,texture); // соберем все в 1 объект
				global.geometry.allSegment.push(obgect);
				global.pano.scene.add( obgect );
				SI++;
				if (SI == AllLine*SegmentLine){cbFunct(SI);}
			}
		}
	}

	function SetPano(dir){
		var url  = dir+'/pano/pano.json';
		var file = hw_ajax({url:url,fc:CreatePano,convert:'JSON',err:errCreatePano});
		THREE.Cache.clear();
		global.pano.camera.rotation.y = 0;
		global.pano.camera.rotation.x = 0;

		function CreatePano(info){
			if (info.type == "sphere"){
				var line = info.line || 4;
				var seg = info.segment || 8;
				var vertex = info.vertex || 16;
				CteateSphere({
					line:line, // количество линий
					segment:seg, // количество сегментов в 1й линии
					vertex:vertex, // количество точкер в 1м сегменте
					cb:SetImg // функция после завершения цикла сосздания сферы
				});
			}
		}
		
		function errCreatePano(){
			var line = 4;
			var seg = 8;
			var vertex = 16;
			CteateSphere({
				line:line, // количество линий
				segment:seg, // количество сегментов в 1й линии
				vertex:vertex, // количество точкер в 1м сегменте
				cb:errSetImg // функция после завершения цикла сосздания сферы
			});
		}
		
		function SetImg(numb){
			THREE.Cache.clear();
			var allImg = [];
			var ii = 1;
			while (ii <= numb) {
				allImg.push(dir+'/pano/'+ii+'.jpg');
			ii++;
			}
			
			loadText();
			function loadText(i=0){
				if (global.geometry.allSegment[i]){
					if (allImg[i]){
						var bufImg = new THREE.TextureLoader().load( 
							allImg[i],
							function(){
								bufImg.minFilter = THREE.LinearFilter; 
								global.geometry.allSegment[i].material.color.setHex( 0xffffff );
								global.geometry.allSegment[i].material.opacity = 1;
								global.geometry.allSegment[i].material.map = bufImg;
								global.geometry.allSegment[i].material.needsUpdate = true;
								if (global.delSeg[i]) {global.pano.scene.remove( global.delSeg[i] );}
								i++;
								loadText(i); 
								//setTimeout(function(){loadText(i)},100);
							},
							"",
							function(){
								console.log("ERROR LOAD "+allImg[i]);
								global.geometry.allSegment[i].material.color = {r:Math.random(),g:Math.random(),b:Math.random()};
								i++;
								loadText(i); 
								//setTimeout(function(){loadText(i)},100);
							}
						);
					}
				}
				else {
					if (global.delSeg){
						var ii=0;
						while (ii <= global.delSeg.length) {
							global.pano.scene.remove( global.delSeg[ii] );
						ii++;
						}
						global.delSeg = [];
					}
				}
			}
		}
		
		function errSetImg(numb){
			if (global.delSeg){
				var ii=0;
				while (ii <= global.delSeg.length) {
					global.pano.scene.remove( global.delSeg[ii] );
				ii++;
				}
				global.delSeg = [];
			}
			for (var i = 0; i < numb ; i++){
				global.geometry.allSegment[i].material.color = {r:Math.random(),g:Math.random(),b:Math.random()};
				global.geometry.allSegment[i].material.opacity = 1;
				global.geometry.allSegment[i].material.needsUpdate = true;
			}
		}
		
	}
	this.SetPano = SetPano;
	
	function ClearPano(){
		THREE.Cache.clear();
		if (global.geometry.allSegment){
			var ii=0;
			while (ii <= global.geometry.allSegment.length) {
				global.pano.scene.remove( global.geometry.allSegment[ii] );
			ii++;
			}
		}
		if (global.delSeg){
			var ii=0;
			while (ii <= global.delSeg.length) {
				global.pano.scene.remove( global.delSeg[ii] );
			ii++;
			}
		}
	}
	this.ClearPano = ClearPano;

	function NextPano(dir){
		THREE.Cache.clear();
		SetPano(dir);
		SetTarget(dir);
	}
	this.set = NextPano;
	
	var allTargetFanct = {};
	object.TargetFunc = {};
	function SetTarget (dir){
		THREE.Cache.clear();
		ClearTarget();
		allTargetFanct = {};
		var url  = dir+'/target/target.json';
		var file = hw_ajax({url:url,fc:CreateTarget,convert:'JSON'});
		
		function CreateTarget(info){
			ClearTarget();
			var frag = document.createDocumentFragment();
			var allT = info || 0;
			if (allT){
				for (var key in allT){
					var elem  = global.doc.createElement('div');
					var t = parseInt(allT[key].top) || 0, l = parseInt(allT[key].left) || 0;
						elem.classList.add("Hw-Three-pano-target-one");
						if (allT[key].class){
							if (allT[key].class.indexOf(' ') != -1){
								var arr = allT[key].class.split(' ');
								arr.forEach(function(item){elem.classList.add(item);});
							}
							else {elem.classList.add(allT[key].class);}
						}
						if (allT[key].css) {elem.style.cssText = allT[key].css;}
						if (allT[key].height) {elem.style.height = allT[key].height+'px';}
						if (allT[key].width) {elem.style.width = allT[key].width+'px';}
						elem.style.position = "absolute";
						elem.style.top = "-8000px";
						elem.style.left = "-8000px";
						
					if (allT[key].content) {
						elem.innerHTML = allT[key].content;
					}
						
					if (allT[key].img){
						var elemImg  = global.doc.createElement('img');
							elemImg.src = dir+'/target/'+allT[key].img.src;
							if (allT[key].img.css){elemImg.style.cssText = allT[key].img.css;}
						elem.appendChild(elemImg);
					}
					
					if (allT[key].nextPano){
						AddLisnerNextPano(elem,allT[key].nextPano);
					}
					
					if (allT[key].func){
						var func = allT[key].func;
						if (!object.TargetFunc[func]) {object.TargetFunc[func]=function(e){};}
						AddLisnerTargetFunc(elem,func);
					}
					
					if (allT[key].data){
						elem.dataset.pin = allT[key].data;
					}

					frag.appendChild(elem);
					global.allTarget.push({e:elem,v:allT[key].vect,t:t,l:l});
				}
			global.pano.panoTarget.appendChild(frag);
			}
		}		
	}
	this.SetTarget = SetTarget;
	
	function ClearTarget(){
		global.pano.panoTarget.innerHTML = "";
		global.allTarget = [];
	}
	this.ClearTarget = ClearTarget;
	
	function AddLisnerNextPano(e,next){
		e.addEventListener('pointerdown',function(){NextPano(next);});
	}
	
	function AddLisnerTargetFunc(e,func){
		e.addEventListener('pointerdown',function(){
			object.TargetFunc[func](e);
		});
	}

	function Animate() {
		PositionTarget();
		global.pano.renderer.render(global.pano.scene, global.pano.camera);
		requestAnimationFrame(Animate);
	};

	function UnScreenPosition(){
		var vector3 = new THREE.Vector3(0,0,0.9);   

		vector3.unproject(global.pano.camera);
		
		var spriteMap = new THREE.TextureLoader().load( "img/pixel.jpg" );
		var spriteMaterial = new THREE.SpriteMaterial( { map: spriteMap, color: 0xffffff } );
		var sprite = new THREE.Sprite( spriteMaterial );
			sprite.position.set(vector1.x,vector1.y,vector1.z);
			
		var spriteMap2 = new THREE.TextureLoader().load( "img/gomer.jpg" );
		var spriteMaterial2 = new THREE.SpriteMaterial( { map: spriteMap2, color: 0xffffff } );
		var sprite2 = new THREE.Sprite( spriteMaterial2 );
			sprite2.position.set(vector3.x,vector3.y,vector3.z);
		
		global.pano.scene.add( sprite );
		global.pano.scene.add( sprite2 );
	}

	function ScreenPosition(v={}){
		var x = v.x || 0, y = v.y || 0, z = v.z || 0;
		var vector = new THREE.Vector3(x,y,z);
		vector.project(global.pano.camera);
		vector.x = ( vector.x * global.canvas.width2 ) + global.canvas.width2;
		vector.y = - ( vector.y * global.canvas.height2 ) + global.canvas.height2;
		return { 
			x: vector.x,
			y: vector.y,
			z: vector.z
		}
	};

	function PositionTarget () { // Функция котороя распологает HTML объекты отнасительно указанных векторов
		if (typeof(global.allTarget) == "object"){
			global.allTarget.forEach(function (item){
				var pos = ScreenPosition(item.v);
				//console.log(item);
				var t = item.t || 0, l = item.l || 0;
				if (pos.z <= 1){
					item.e.style.top = (pos.y+t)+"px";
					item.e.style.left = (pos.x+l)+"px";
				}
			});
		}
	}

	/* Click function */
	function CanvasClick(e){ // функция при клике на канвас
		if (!global.click) {global.click = {};}
		global.click.sx = e.pageX || e.touches[0].pageX;
		global.click.sy = e.pageY || e.touches[0].pageY;
		global.click.rotationX = global.pano.camera.rotation.y
		global.click.rotationY = global.pano.camera.rotation.x
		
		if (e.touches && e.touches.length == 2) {
			CanvasEnd();
			global.click.zoomPano = global.pano.camera.zoom;
			global.click.delta = Math.sqrt(Math.pow(e.touches[1].pageX-e.touches[0].pageX,2)+Math.pow(e.touches[1].pageY-e.touches[0].pageY,2));
			global.canvas.addEventListener('touchmove',CameraZoom);
		} else {
			global.canvas.addEventListener('mousemove',CanvasMove);
			global.canvas.addEventListener('touchmove',CanvasMove);
			
			global.doc.addEventListener('mouseup',CanvasEnd);
			global.canvas.addEventListener('touchend',CanvasEnd);
		}
	}

	function CanvasMove(e){ // функция движения пальца или мыши по канвасу
		var rotationSX = global.click.rotationX;
		var rotationSY = global.click.rotationY;
		var x = e.pageX || e.touches[0].pageX;
		var y = e.pageY || e.touches[0].pageY;
		var mx = global.click.sx - x;
		var my = global.click.sy - y;
		
		var rotationX = (rotationSX-(mx*global.control.rotationSpeed)); 
		var rotationY = (rotationSY-(my*global.control.rotationSpeed)); 
		if (rotationY >= global.control.yMax) {rotationY = global.control.yMax;} 
		if (rotationY <= -global.control.yMax) {rotationY = -global.control.yMax;}
		global.pano.camera.rotation.y = rotationX;
		global.pano.camera.rotation.x = rotationY;
	}

	function CanvasEnd(){ // функция когда отпустили мыш или канвас
		global.canvas.removeEventListener('mousemove',CanvasMove);
		global.canvas.removeEventListener('touchmove',CanvasMove);
		global.canvas.removeEventListener('touchmove',CameraZoom);
	}

	function CameraZoom (e){ // увелечение панорамы 2мя пальцами
		var x1 = e.touches[0].pageX;
		var x2 = e.touches[1].pageX;
		var y1 = e.touches[0].pageY;
		var y2 = e.touches[1].pageY;
		
		var delta = (Math.sqrt(Math.pow(x2-x1,2)+Math.pow(y2-y1,2)))-global.click.delta;
		var sz  = global.click.zoomPano+(delta*global.control.zoomTouchKof);
		
		UpdateZoom(sz);
	}

	function CameraWheel(e){ // увелечение панорамы колесиком мышки
		e.preventDefault ? e.preventDefault() : (e.returnValue = false);
		var delta = e.deltaY || e.detail || e.wheelDelta;
		var zoomPano = global.pano.camera.zoom;
		var sz  = global.pano.camera.zoom-(delta*global.control.zoomMousKof);
		UpdateZoom(sz);
	}
	
	this.zoom = UpdateZoom;
	
	function UpdateZoom(sz){
		if (sz >= global.control.zoomMax){sz=global.control.zoomMax;} if (sz <= global.control.zoomMin){sz=global.control.zoomMin;}
		global.pano.camera.zoom = sz;
		global.pano.camera.updateProjectionMatrix();
		zoomFunct(sz);
	}
	

/* -Click function- */
	function hw_disable_touch(elem){
		function disable_touch_event (event){
			event.preventDefault(); // Отменяет событие, если оно отменяемое
			event.stopPropagation(); // Прекращает дальнейшую передачу текущего события.
		}
		elem.addEventListener('touchstart',disable_touch_event);
		elem.addEventListener('touchmove',disable_touch_event);
		elem.addEventListener('touchend',disable_touch_event);
	}
	
	this.getUnScreenPosition = getUnScreenPosition;
	function getUnScreenPosition(camera){
		var vector = new THREE.Vector3(0,0,0.9);   
		vector.unproject(camera);
		return vector;
	};
	
	this.getScreenPosition = getScreenPosition;
	function getScreenPosition(v,camera,width2,height2){
		var x = v.x || 0, y = v.y || 0, z = v.z || 0;
		var vector = new THREE.Vector3(x,y,z);
		vector.project(camera);
		vector.x = ( vector.x * width2 ) + width2;
		vector.y = - ( vector.y * height2 ) + height2;
		return { 
			x: vector.x,
			y: vector.y,
			z: vector.z
		}
	};
	
	function hw_ajax (option){
		var XHR = ("onload" in new XMLHttpRequest()) ? XMLHttpRequest : XDomainRequest;
		
		var url = option.url || '';
		var type = option.type || 'GET'; 
		var convert = option.convert || '';
		var fc = option.fc || function(){};
		var err = option.err || function(){};
		
		var dataInfo = 'None date!!';

		var request = new XHR();
		request.onreadystatechange = function() {
			if(request.readyState === 4) {
				if(request.status === 200) { 
					dataInfo = request.responseText
					if (convert == 'JSON') {dataInfo = JSON.parse(dataInfo)}
					fc(dataInfo);
				} else {err();}
			}
		}
		var ver = new Date().getTime();
		request.open(type, url+"?ver="+ver);
		request.send();
	}
}