<?
$name = $_POST['name'];
$line = $_POST['line'];
$segment = $_POST['segment'];
$vertex = $_POST['vertex'];
$width = $_POST['width'];
$height = $_POST['height'];

function imageresize($outfile,$infile,$neww,$newh,$quality) {

    $im=imagecreatefromjpeg($infile);
    $im1=imagecreatetruecolor($neww,$newh);
    imagecopyresampled($im1,$im,0,0,0,0,$neww,$newh,imagesx($im),imagesy($im));

    imagejpeg($im1,$outfile,$quality);
    imagedestroy($im);
    imagedestroy($im1);
}

if ($name) {
	if(!is_dir('pano')){ mkdir('pano');}
	if(!is_dir('pano/'.$name)){ 
		if(mkdir('pano/'.$name)){
			$urlPano = 'pano/'.$name.'/pano'; 
			$urlPanoJson = $urlPano.'/pano.json';
			$urltarget ='pano/'.$name.'/target'; 
			$urltargetJson = $urltarget.'/target.json';
			mkdir($urlPano);
			mkdir($urltarget);
			
			$panoJsonContent = array('type'=>'sphere','line'=>$line,'segment'=>$segment,'vertex'=>$vertex);
			$panoJsonContent = json_encode($panoJsonContent);
			$targetJsonContent = '{}';
			
			file_put_contents($urlPanoJson,$panoJsonContent);
			file_put_contents($urltargetJson,$targetJsonContent);
			
			$w = $width;//8192;
			$h = $width/$height;//4096;
			$sw = $w/$segment;
			$sh = $h/$line;
			$imgStart = imagecreatefromjpeg($_FILES['panoImg']['tmp_name']);
			$imgFul=imagecreatetruecolor($w,$h);
			imagecopyresampled($imgFul,$imgStart,0,0,0,0,$w,$h,imagesx($imgStart),imagesy($imgStart));
			
			$x = 0; $y = 0;	$numb = 0;
			for($il=0; $il < $line; $il++){
				$y = $il*$sh;
				for($is=0; $is < $segment; $is++){
					$numb++;
					$imgSegment = imagecreatetruecolor($sw, $sh);
					$x = $is*$sw;
					imagecopy($imgSegment,$imgFul,0,0,$x,$y,$sw,$sh);
					imagejpeg($imgSegment,$urlPano.'/'.$numb.'.jpg',100); // вывод потока
					imagedestroy($imgSegment);
				}
			}
			imagedestroy($imgStart);
			imagedestroy($imgFul);
		}
	}
	header('Location:?name='.$name);exit;
}
?>