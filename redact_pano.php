<a href="?page=main">Main page</a>
<div class="pano">
	<div class="target_center">
		<img src="img/target_skin/cross.png" />
		<img class="target_boom" src="img/boom/boom_3.gif" data-time=1 />
	</div>

	<div class="target_settings">
		<div class="target_top"><p>Top</p><span>:</span><input name="target_top" type="text" value="-50" /></div>
		<div class="target_left"><p>Left</p><span>:</span><input name="target_left" type="text" value="-50" /></div>
		<div class="target_width"><p>Width</p><span>:</span><input name="target_width" type="text" value="100" /></div>
		<div class="target_height"><p>Height</p><span>:</span><input name="target_height" type="text" value="100" /></div>
		<br>
		<div class="target_class"><p>Class</p><span>:</span><input name="target_class" type="text" /></div>
		<div class="target_data"><p>Data</p><span>:</span><input name="target_data" type="text" /></div>
		<br>
		<div class="target_css"><p>Css</p><span>:</span><textarea name="target_css" >background:#000;</textarea></div>
		<div class="target_content"><p>Content</p><span>:</span><textarea name="target_content"></textarea></div>
		<br>
		<div class="target_img_src"><p>Img src</p><span>:</span><input name="target_img_src" type="text" /></div>
		<div class="target_img_css"><p>Img css</p><span>:</span><input name="target_img_css" type="text" /></div>
		<br>
		<div class="target_nextPano"><p>Next Pano</p><span>:</span><input name="target_nextPano" type="text" /></div>
		<div class="target_func"><p>Function</p><span>:</span><input name="target_func" type="text" /></div>
	</div>

</div>

<div class="click">Add Target</div>
<div class="click">Settings Target</div>
<div class="click">Download</div>
<div class="click">Save</div>